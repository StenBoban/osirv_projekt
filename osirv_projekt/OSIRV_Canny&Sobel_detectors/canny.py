import scipy.ndimage as ndi
import scipy
import numpy as np
from PIL import Image
import math
from math import pi
import matplotlib.pyplot as plt


sigma = 2.2

#ucitavanje slike

#f = 'C:/Users/Sten/Desktop/sad/dancing-spider2.jpg'
f = '/home/student/Desktop/osirv/dancing-spider1.jpg'


# pretvorba slike u sive tonove
img = Image.open(f).convert('L')
imgdata = np.array(img, dtype = float)                                 

scipy.misc.imsave('Sivi_tonovi.jpg', imgdata)

# primjenjivanje gaussovog filtera na sliku (koja je prethodno prebacena u sive tonove)
# ovim korakom se vrsi izgladivanje slike, cime se znacajno smanjuju sumovi                      
G = ndi.filters.gaussian_filter(img, sigma)

scipy.misc.imsave('Primjena_gaussovog_filtera.jpg', G)

sobelout = np.array(Image.new('L', img.size)).astype(np.uint8)                                       #empty image
Gx = np.array(sobelout, dtype = float)                        
Gy = np.array(sobelout, dtype = float)

sobel_x = [[-1,0,1],
           [-2,0,2],
           [-1,0,1]]
sobel_y = [[-1,-2,-1],
           [0,0,0],
           [1,2,1]]

width = img.size[1]
height = img.size[0]

# odredivanje intenziteta gradijenta slike i smjera pruzanja piksela

for x in range(1, width-1):
    for y in range(1, height-1):
        px = (sobel_x[0][0] * G[x-1][y-1]) + (sobel_x[0][1] * G[x][y-1]) + \
             (sobel_x[0][2] * G[x+1][y-1]) + (sobel_x[1][0] * G[x-1][y]) + \
             (sobel_x[1][1] * G[x][y]) + (sobel_x[1][2] * G[x+1][y]) + \
             (sobel_x[2][0] * G[x-1][y+1]) + (sobel_x[2][1] * G[x][y+1]) + \
             (sobel_x[2][2] * G[x+1][y+1])

        py = (sobel_y[0][0] * G[x-1][y-1]) + (sobel_y[0][1] * G[x][y-1]) + \
             (sobel_y[0][2] * G[x+1][y-1]) + (sobel_y[1][0] * G[x-1][y]) + \
             (sobel_y[1][1] * G[x][y]) + (sobel_y[1][2] * G[x+1][y]) + \
             (sobel_y[2][0] * G[x-1][y+1]) + (sobel_y[2][1] * G[x][y+1]) + \
             (sobel_y[2][2] * G[x+1][y+1])
        Gx[x][y] = px
        Gy[x][y] = py

# odedivanje intenziteta gradijenta slike
SobelOutGrad = scipy.hypot(Gx, Gy)

# odredivanje smjera pruzanja piksela
SobelOutDirection = scipy.arctan2(Gy, Gx)

scipy.misc.imsave('Canny_gradijent.jpg', SobelOutGrad)
scipy.misc.imsave('Canny_smjer_piksela.jpg', SobelOutDirection)

# potrebno je zaokruziti vrijednost kuta, kojim se pruza piksel, na 0, 45, 90 ili 135
for x in range(width):
    for y in range(height):
        if (SobelOutDirection[x][y]<22.5 and SobelOutDirection[x][y]>=0) or \
           (SobelOutDirection[x][y]>=157.5 and SobelOutDirection[x][y]<202.5) or \
           (SobelOutDirection[x][y]>=337.5 and SobelOutDirection[x][y]<=360):
            SobelOutDirection[x][y]=0
        elif (SobelOutDirection[x][y]>=22.5 and SobelOutDirection[x][y]<67.5) or \
             (SobelOutDirection[x][y]>=202.5 and SobelOutDirection[x][y]<247.5):
            SobelOutDirection[x][y]=45
        elif (SobelOutDirection[x][y]>=67.5 and SobelOutDirection[x][y]<112.5)or \
             (SobelOutDirection[x][y]>=247.5 and SobelOutDirection[x][y]<292.5):
            SobelOutDirection[x][y]=90
        else:
            SobelOutDirection[x][y]=135


scipy.misc.imsave('Canny_smjer_zaokruzenih_piksela.jpg', SobelOutDirection)

# matrica intenziteta
NonMaxSupp = SobelOutGrad.copy()

# primjena non-maximum suppression algoritma
for x in range(1, width-1):
    for y in range(1, height-1):
        if SobelOutDirection[x][y]==0:
            if (SobelOutGrad[x][y]<=SobelOutGrad[x][y+1]) or \
               (SobelOutGrad[x][y]<=SobelOutGrad[x][y-1]):
                NonMaxSupp[x][y]=0
        elif SobelOutDirection[x][y]==45:
            if (SobelOutGrad[x][y]<=SobelOutGrad[x-1][y+1]) or \
               (SobelOutGrad[x][y]<=SobelOutGrad[x+1][y-1]):
                NonMaxSupp[x][y]=0
        elif SobelOutDirection[x][y]==90:
            if (SobelOutGrad[x][y]<=SobelOutGrad[x+1][y]) or \
               (SobelOutGrad[x][y]<=SobelOutGrad[x-1][y]):
                NonMaxSupp[x][y]=0
        else:
            if (SobelOutGrad[x][y]<=SobelOutGrad[x+1][y+1]) or \
               (SobelOutGrad[x][y]<=SobelOutGrad[x-1][y-1]):
                NonMaxSupp[x][y]=0

scipy.misc.imsave('Non_max_suppression.jpg', NonMaxSupp)

# uzimanje najvece vrijednosti iz matrice intenziteta
m = np.max(NonMaxSupp)

# postavljanje rubova (gornji i donji)
T_high = 0.2*m
T_low = 0.1*m

# pravljenje praznih slika, gdje gnh predstavlja sliku s gornjim (jakim) piksleima, a gnl s donjim (slabim)
gnh = np.zeros((width, height))
gnl = np.zeros((width, height))

for x in range(width):
    for y in range(height):
        if NonMaxSupp[x][y]>=T_high:
            gnh[x][y]=NonMaxSupp[x][y]
        if NonMaxSupp[x][y]>=T_low:
            gnl[x][y]=NonMaxSupp[x][y]

scipy.misc.imsave('Prije_normalizacije_rubova.jpg', gnl)

# normalizacija rubova
gnl = gnl-gnh

scipy.misc.imsave('Gradijent_nakon_normalizacije_rubova.jpg', gnl)
scipy.misc.imsave('Gornji(jaki)_pikseli.jpg', gnh)


def traverse(i, j):
    x = [-1, 0, 1, -1, 1, -1, 0, 1]
    y = [-1, -1, -1, 0, 0, 1, 1, 1]
    for k in range(8):
        if gnh[i+x[k]][j+y[k]]==0 and gnl[i+x[k]][j+y[k]]!=0:
            gnh[i+x[k]][j+y[k]]=1
            traverse(i+x[k], j+y[k])

for i in range(1, width-1):
    for j in range(1, height-1):
        if gnh[i][j]:
            gnh[i][j]=1
            traverse(i, j)


scipy.misc.imsave('Jaki_pikseli_kraj.jpg', gnh)

