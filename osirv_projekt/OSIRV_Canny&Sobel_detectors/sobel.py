import numpy as np
from PIL import Image
import matplotlib.pyplot as plt

# ucitavanje slike
img = np.array(Image.open('/home/student/Desktop/osirv/dancing-spider2.jpg')).astype(np.uint8)

# pretvorba slike u sive tonove
gray_img = np.round(0.299 * img[:, :, 0] +
                    0.587 * img[:, :, 1] +
                    0.114 * img[:, :, 2]).astype(np.uint8)

# Sobel Operator
h, w = gray_img.shape
# definiramo horizontalni i vertikalni filter koji se kod sobel operatora primjenjuje na sliku
horizontal = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
vertical = np.array([[-1, -2, -1], [0, 0, 0], [1, 2, 1]])

# definiranje slika s nulama
newhorizontalImage = np.zeros((h, w))
newverticalImage = np.zeros((h, w))
newgradientImage = np.zeros((h, w))

# pomak za 1 horizontalno i vertikalno
for i in range(1, h - 1):
    for j in range(1, w - 1):
        horizontalGrad = (horizontal[0, 0] * gray_img[i - 1, j - 1]) + \
                         (horizontal[0, 1] * gray_img[i - 1, j]) + \
                         (horizontal[0, 2] * gray_img[i - 1, j + 1]) + \
                         (horizontal[1, 0] * gray_img[i, j - 1]) + \
                         (horizontal[1, 1] * gray_img[i, j]) + \
                         (horizontal[1, 2] * gray_img[i, j + 1]) + \
                         (horizontal[2, 0] * gray_img[i + 1, j - 1]) + \
                         (horizontal[2, 1] * gray_img[i + 1, j]) + \
                         (horizontal[2, 2] * gray_img[i + 1, j + 1])

        # spremanje nove slike (nakon prolaska horizontalnim filterom kroz sliku)
        newhorizontalImage[i - 1, j - 1] = abs(horizontalGrad)

        verticalGrad = (vertical[0, 0] * gray_img[i - 1, j - 1]) + \
                       (vertical[0, 1] * gray_img[i - 1, j]) + \
                       (vertical[0, 2] * gray_img[i - 1, j + 1]) + \
                       (vertical[1, 0] * gray_img[i, j - 1]) + \
                       (vertical[1, 1] * gray_img[i, j]) + \
                       (vertical[1, 2] * gray_img[i, j + 1]) + \
                       (vertical[2, 0] * gray_img[i + 1, j - 1]) + \
                       (vertical[2, 1] * gray_img[i + 1, j]) + \
                       (vertical[2, 2] * gray_img[i + 1, j + 1])

        # spremanje nove slike (nakon prolaska vertikalnim filterom kroz sliku)
        newverticalImage[i - 1, j - 1] = abs(verticalGrad)

        # izrazavanje rubova, odnosno konacni rezultat provodenja sobel detektora
        mag = np.sqrt(pow(horizontalGrad, 2.0) + pow(verticalGrad, 2.0))
        newgradientImage[i - 1, j - 1] = mag

plt.figure()
plt.imshow(newgradientImage, cmap='gray')
plt.show()